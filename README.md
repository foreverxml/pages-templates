# Pages Templates

Find source [here](https://codeberg.org/foreverxml/pages-src)

The directory which I hold my shortcodes and templates. If you want to add more, create a PR with more templates. This is under the Unlicense, while the rest of my code is under the AGPL. You can copy this, but please do not copy my page source.